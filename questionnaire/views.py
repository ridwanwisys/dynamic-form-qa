from django.views.generic import ListView, CreateView, UpdateView
from django.core.context_processors import csrf
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse

from extra_views import FormSetView,CreateWithInlinesView, UpdateWithInlinesView, NamedFormsetsMixin
#from extra_views.generic import GenericInlineFormSet

from .forms import AnswerInline, QuestionInline, QuestionSetForm, QuestionForm, AnswerForm
from .models import Questionset, Question, Answer

class QuestionSetListView(ListView):
    model = Questionset


class AnswerListView(ListView):
    model = Answer


class QuestionListView(ListView):
    model = Question


class QuestionSetCreateView(NamedFormsetsMixin, CreateWithInlinesView):
    model = Questionset
    form_class = QuestionSetForm
    inlines = [QuestionInline]
    inlines_names = ['QuestionInline']
    context_object_name = 'questionset'

    def get_success_url(self):
        return self.object.get_absolute_url()


class QuestionSetUpdateView(NamedFormsetsMixin, UpdateWithInlinesView):
    model = Questionset
    form_class = QuestionSetForm
    inlines = [QuestionInline]
    inlines_names = ['QuestionInline']
    context_object_name = 'questionset'

    def get_success_url(self):
        return self.object.get_absolute_url()


class QuestionCreateView(NamedFormsetsMixin, CreateWithInlinesView):
    model = Question
    form_class = QuestionForm
    inlines = [AnswerInline]
    inlines_names = ['AnswerInline']
    context_object_name = 'question'

    def get_success_url(self):
        return self.object.get_absolute_url()


class QuestionUpdateView(NamedFormsetsMixin, UpdateWithInlinesView):
    model = Question
    form_class = QuestionForm
    inlines = [AnswerInline]
    inlines_names = ['AnswerInline']
    context_object_name = 'question'

    def get_success_url(self):
        return self.object.get_absolute_url()



from .forms import RequiredFormSet
from django.forms.formsets import formset_factory, BaseFormSet
from django.forms.models import inlineformset_factory


class MultiQuestionCreateView(CreateView):

    QuestionFormSet = formset_factory(QuestionForm, max_num=None, formset=RequiredFormSet)
    AnswerFormSet = inlineformset_factory(Question, Answer, form=AnswerForm, formset=RequiredFormSet, max_num=None)

    def get(self,request, *args, **kwargs):
        question_formset = self.QuestionFormSet()
        answer_formset = self.AnswerFormSet()
        c = {'question_formset': question_formset,
             'answer_formset': answer_formset,
        }
        c.update(csrf(request))
        return render_to_response('questionnaire/test_question_form.html', c)

    def post(self,request,*args,**kwargs):
        question_form = self.QuestionFormSet(request.POST)
        answer_formset = self.AnswerFormSet(request.POST, request.FILES)

        """
        if question_form.is_valid() and answer_formset.is_valid():
            question = question_form.save()
            for form in answer_formset.forms:
                answer = form.save(commit=False)
                answer.question = question
                answer.save()
        """

        print 'Here comes the post'
        print request.POST
        print 'Here ends the post'
        return redirect('questions-list')


    def get_success_url(self):
        return reverse('questions-list')


from nested_formset import nested_formset_factory

class EditMultiQuestionsView(UpdateView):
    model = Questionset

    qa_nested_formset = nested_formset_factory(
            Questionset,
            Question,
            Answer,
        )

    def get_template_names(self):
        return ['questionnaire/test_question_form.html']

    def get_form_class(self):
        return self.qa_nested_formset

    """def post(self,request, *args, **kwargs):
                    print 'Here comes the post'
                    print request.POST
                    print 'Here ends the post'
                    return redirect('questions-list')"""

    def get_success_url(self):
        return reverse('questions-list')















