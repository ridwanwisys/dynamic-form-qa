from django.db import models
from django.core.urlresolvers import reverse


########  Question - Answer    ###########



class Questionset(models.Model):
    description =  models.CharField(max_length=255)

    def __unicode__(self):
        return self.description

    def get_absolute_url(self):
        #return reverse('questionsets-list', args=[self.id])
        return reverse('questionsets-list')


class Question(models.Model):
    questionset = models.ForeignKey(Questionset)
    ques = models.CharField(max_length=255)

    def __unicode__(self):
        return self.ques

    def get_absolute_url(self):
        #return reverse('questions-list', args=[self.id])
        return reverse('questions-list')


from django.contrib.contenttypes.models import ContentType

class Answer(models.Model):
    question = models.ForeignKey(Question)
    answer =  models.CharField(max_length=255)
    #content_type = models.ForeignKey(ContentType)
    #object_id = models.PositiveIntegerField()

    def __unicode__(self):
        return self.answer
