from django import forms
from django.forms.formsets import formset_factory, BaseFormSet

from extra_views import InlineFormSet

from .models import Questionset, Question, Answer


class RequiredFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class QuestionSetForm(forms.ModelForm):
    class Meta:
        model = Questionset


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        exclude = ('questionset',)



class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        exclude = ('question',)



class QuestionInline(InlineFormSet):
    model = Question
    extra = 3
    form_class = QuestionForm



class AnswerInline(InlineFormSet):
    model = Answer
    extra = 3
    form_class = AnswerForm





