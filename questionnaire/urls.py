from django.conf.urls import patterns, url

from questionnaire import views

urlpatterns = patterns('',
    url(r'^questionsets/$', views.QuestionSetListView.as_view(), name='questionsets-list'),
    url(r'^questionsets/new/$', views.QuestionSetCreateView.as_view(), name='questionsets-new'),
    url(r'^questionsets/(?P<pk>\d+)/$', views.QuestionSetUpdateView.as_view(), name='questionsets-edit'),

    url(r'^questions/$', views.QuestionListView.as_view(), name='questions-list'),
    url(r'^questions/new/$', views.QuestionCreateView.as_view(), name='questions-new'),
    url(r'^questions/(?P<pk>\d+)/$', views.QuestionUpdateView.as_view(), name='questions-edit'),

    url(r'^multiquestions/(?P<pk>\d+)/$', views.EditMultiQuestionsView.as_view() , name='multiquestions-new'),

    url(r'^answers/$', views.AnswerListView.as_view(), name='answers-list'),
)
