from django.db import models


class TodoList(models.Model):
    list_name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.list_name

    def get_absolute_url(self):
        return reverse('todo_list')


class TodoItem(models.Model):
    item_name = models.CharField(max_length=150, help_text="e.g. Buy milk, wash dog etc")
    todo_list = models.ForeignKey(TodoList)

    def __unicode__(self):
        return self.item_name + " (" + str(self.todo_list) + ")"

    def get_absolute_url(self):
        return reverse('todo_list')


