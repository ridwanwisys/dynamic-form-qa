from django.shortcuts import render_to_response
from django.core.context_processors import csrf
from django.template import RequestContext
from django.forms.formsets import formset_factory, BaseFormSet
from django.http import HttpResponse, HttpResponseRedirect

from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
)

from todo.forms import *
from todo import models

def index(request):
    # This class is used to make empty formset forms required
    # See http://stackoverflow.com/questions/2406537/django-formsets-make-first-required/4951032#4951032
    class RequiredFormSet(BaseFormSet):
        def __init__(self, *args, **kwargs):
            super(RequiredFormSet, self).__init__(*args, **kwargs)
            for form in self.forms:
                form.empty_permitted = False

    TodoItemFormSet = formset_factory(TodoItemForm, max_num=None, formset=RequiredFormSet)

    if request.method == 'POST': # If the form has been submitted...
        todo_list_form = TodoListForm(request.POST) # A form bound to the POST data
        # Create a formset from the submitted data
        todo_item_formset = TodoItemFormSet(request.POST, request.FILES)

        if todo_list_form.is_valid() and todo_item_formset.is_valid():
            todo_list = todo_list_form.save()
            for form in todo_item_formset.forms:
                todo_item = form.save(commit=False)
                todo_item.todo_list = todo_list
                todo_item.save()

            return HttpResponseRedirect('todo_list') # Redirect to a 'success' page
    else:
        todo_list_form = TodoListForm()
        todo_item_formset = TodoItemFormSet()

    # For CSRF protection
    # See http://docs.djangoproject.com/en/dev/ref/contrib/csrf/
    c = {'todo_list_form': todo_list_form,
         'todo_item_formset': todo_item_formset,
        }
    c.update(csrf(request))

    return render_to_response('todo/index.html', c)


from extra_views import FormSetView,CreateWithInlinesView, UpdateWithInlinesView, NamedFormsetsMixin
from extra_views.generic import GenericInlineFormSet


class TodoListView(ListView):
    model = models.TodoList


class TodoListCreateView(NamedFormsetsMixin, CreateWithInlinesView):
    model = models.TodoList
    form_class = TodoListForm
    inlines = [TodoItemForm]
    inlines_names = ['TodoItem']
    context_object_name = 'TodoList'

    def get_success_url(self):
        return self.object.get_absolute_url()







