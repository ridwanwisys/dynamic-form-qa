from django.conf.urls import patterns, url

from todo import views

urlpatterns = patterns('',
    url(r'^$', 'todo.views.index', name='home'),

    url(r'^todo_list/$', views.TodoListView.as_view(), name='todo_list'),
    url(r'^todo_list/new/$', views.TodoListCreateView.as_view(), name='create_todo_list'),

)
