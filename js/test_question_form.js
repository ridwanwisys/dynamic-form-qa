$(document).ready(function() {
  // Code adapted from http://djangosnippets.org/snippets/1389/

  function updateElementIndex(el, prefix, ndx) {
    var id_regex = new RegExp('(' + prefix + '-\\d+-)');
    var replacement = prefix + '-' + ndx + '-';
    if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex,
 replacement));
    if (el.id) el.id = el.id.replace(id_regex, replacement);
    if (el.name) el.name = el.name.replace(id_regex, replacement);
  }

  function deleteQuestionForm(btn, prefix) {
    var formCount = parseInt($('#id_question_set' + '-TOTAL_FORMS').val());

    //console.log(formCount);

    if (formCount > 1) {
      // Delete the item/form
      var removeItem = $(btn).parents('.item');
      $(removeItem).slideUp(500, function(){ $(removeItem).remove(); });;

      //var forms = $('#question-div'); // Get all the forms
      //console.log(forms.length);

      // Update the total number of forms (1 less than before)
      $('#id_question_set' + '-TOTAL_FORMS').val(formCount-1);

/*
      var i = 0;
      // Go through the forms and set their indices, names and IDs
      for (formCount = forms.length; i < formCount; i++) {
        $(forms.get(i)).children().children().each(function() {
          updateElementIndex(this, prefix, i);
        });
      }
*/

    } // End if
    else {
        alert("You have to enter at least one todo item!");
    }
    return false;
  }


  function addQuestionForm(btn, prefix) {
    var formCount = parseInt($('#id_question_set' + '-TOTAL_FORMS').val());

      // Clone a form (without event handlers) from the first form
      var row = $(".hidden:first").clone(false).get(0);

      // Remove the bits we don't want in the new row/form
      // e.g. error messages
      $(".errorlist", row).remove();
      $(row).children().removeClass('error');

      // Relabel/rename all the relevant bits
      $(row).children().children().each(function() {
        updateElementIndex(this, prefix, formCount);
        if ( $(this).attr('type') == 'text' )
          $(this).val('');
      });

      // Insert it after the last form
      $(row).removeAttr('id').hide().insertAfter("#question-items:last").slideDown(1000);

      // Add an event handler for the delete item/form link
      $(row).find('.deleteQuestion').click(function() {
        return deleteQuestionForm(this, prefix);
      });

      $(row).find('#ques-item input').attr('name', 'question_set-'+ formCount +'-ques');
      $(row).find('#ques-item input').attr('id', 'id_question_set-'+ formCount +'-ques');


      $(row).find('#answer-item #id_question_set-__prefix__-answer_set-TOTAL_FORMS').attr('name', 'question_set-'+ formCount +'-answer_set-TOTAL_FORMS');
      $(row).find('#answer-item #id_question_set-__prefix__-answer_set-INITIAL_FORMS').attr('name', 'question_set-'+ formCount +'-answer_set-INITIAL_FORMS');
      $(row).find('#answer-item #id_question_set-__prefix__-answer_set-MAX_NUM_FORMS').attr('name', 'question_set-'+ formCount +'-answer_set-MAX_NUM_FORMS');
      $(row).find('#answer-item #id_question_set-__prefix__-answer_set-0-answer').attr('name', 'question_set-'+ formCount +'-answer_set-0-answer');


      $(row).find('#answer-item #id_question_set-__prefix__-answer_set-TOTAL_FORMS').attr('id', 'id_question_set-'+ formCount +'-answer_set-TOTAL_FORMS');
      $(row).find('#answer-item #id_question_set-__prefix__-answer_set-INITIAL_FORMS').attr('id', 'id_question_set-'+ formCount +'-answer_set-INITIAL_FORMS');
      $(row).find('#answer-item #id_question_set-__prefix__-answer_set-MAX_NUM_FORMS').attr('id', 'id_question_set-'+ formCount +'-answer_set-MAX_NUM_FORMS');
      $(row).find('#answer-item #id_question_set-__prefix__-answer_set-0-answer').attr('id', 'id_question_set-'+ formCount +'-answer_set-0-answer');


      // Update the total form count
      $('#id_' + 'question_set' + '-TOTAL_FORMS').val(formCount + 1);

    return false;
  }

  // Register the click event handlers
  $("#addQuestion").click(function() {
    return addQuestionForm(this, 'form');
  });

  $(".deleteQuestion").click(function() {
    return deleteQuestionForm(this, 'form');
  });


});